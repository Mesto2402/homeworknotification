import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer} from '@react-navigation/native'
import HomeScreen from '../screens/HomeScreen'
import LogInScreen from '../screens/LogInScreen'
import SignUpScreen from '../screens/SignUpScreen'
import CalendarScreen from '../screens/CalendarScreen'
import NFCScreen from '../screens/NFCScreen'
import TasksScreen from '../screens/TasksScreen'
import ProfileScreen from '../screens/ProfileScreen'
import DetailsScreen from '../screens/DetailsScreen'
import TabScreen from '../navigation/TabScreen'

const Stack = createStackNavigator();

export default class MainStack extends React.Component {
  render (){
    return (
      <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Inicio" component={HomeScreen} options={{headerShown: false}}/>
            <Stack.Screen name="HMW" component={LogInScreen} options={{headerShown: false}}/>
            <Stack.Screen name="SignUp" component={SignUpScreen} options={{headerShown: false}}/>

            <Stack.Screen name="Tabs" component={TabScreen} options={{headerShown: false}}/>
          </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
/*
headerStyle:{backgroundColor:'#6495ed'
headerStyle:{backgroundColor:'transparent'}
headerShown: false
            <Stack.Screen name="Profile" component={ProfileScreen} />
            <Stack.Screen name="Calendar" component={CalendarScreen} />
            <Stack.Screen name="Tasks" component={TasksScreen} />
            <Stack.Screen name="NFC" component={NFCScreen} />
            <Stack.Screen name="Details" component={DetailsScreen} />
*/