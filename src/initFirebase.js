import * as firebase from 'firebase';
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyB2pj0KXLslssWmXHSNjeCDyjqEM2P_55c",
    authDomain: "umg-homework.firebaseapp.com",
    databaseURL: "https://umg-homework.firebaseio.com",
    projectId: "umg-homework",
    storageBucket: "umg-homework.appspot.com",
    messagingSenderId: "823284688323",
    appId: "1:823284688323:web:e16f7cd818daf01b914976",
    measurementId: "G-measurement-id"
}

firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore()

export const getResource = resource => {
    return new Promise((resolve, reject) => {
        db.collection(resource)
            .get()
            .then(snapshots => resolve(snapshots.docs.map(doc => doc.data())))
            .catch(err => reject(err))
    })
}

export const getId = (resource, column, value) => {
    return new Promise((resolve, reject) => {
        db.collection(resource)
            .where(column, "==", value)
            .get()
            .then(snapshots => resolve(snapshots.docs.map(doc => doc.id)))
            .catch(err => reject(err))
    })
}

export const deleteDocument = (resource, id) => {
    return new Promise((resolve, reject) => {
        db.collection(resource).doc(id)
            .delete()
            .then(() => resolve(alert("Borrado exitosamente")))
            .catch(err => reject(err))
    })
}