import React from 'react'
import{ View, ImageBackground, Image, StyleSheet, Dimensions, TextInput, ScrollView, TouchableOpacity } from 'react-native'
import { Card, CardItem, Text, Button, Icon, Left, Body, Right, Item, Input, Label, DatePicker } from 'native-base'
import styles from '../styles/Styles'

export default class TasksScreen extends React.Component {
  state = {
    search: '',
  };
  updateSearch = (search) => {
    this.setState({ search });
  };
  constructor(props) {
    super(props);
    this.setDate = this.setDate.bind(this);
  }
  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

  render(){
    const { navigation } = this.props
    const window = Dimensions.get('window')
    const { search } = this.state;

    return (
      <ImageBackground source={require('../images/HWO.jpeg')} style={{width: window.width, height: window.height}}>
        <View
          style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 80            
          }}
        >
          <Image source={require('../images/Titulo.png')} style={{resizeMode: 'stretch', height:70, width:300}}></Image>
        </View>
            <Card style={{ alignSelf:'center', width: 350 }}>
            <CardItem>
                <Body>
                  <Text style={{fontWeight: 'bold', fontSize:20}}>Notificación</Text>
                </Body>
                <Right>
                <Icon name='alarm' style={{fontSize: 26, color:'#6495ED'}}/>
                </Right>
            </CardItem>
            <CardItem>
              <Body>
              <Item stackedLabel style={{backgroundColor:'#6495ED', borderRadius:10, width: 310}}>
                <Label style={{color:'#fff'}}>   Nombre de la materia:</Label>
                <Input style={{color:'#fff'}}/>
              </Item>
              </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Item stackedLabel style={{backgroundColor:'#6495ED', borderRadius:10, width: 310}}>
                  <Label style={{color:'#fff'}}>   Descripción:</Label>
                  <Input style={{color:'#fff'}}/>
                </Item>
              </Body>
            </CardItem>
            <CardItem>
              <Left>
                <Body>
                  <TouchableOpacity>
                  <Button rounded success small>
                    <Text>Fácil</Text>
                  </Button>
                  </TouchableOpacity>
                </Body>
              </Left>
              <Body>
                <TouchableOpacity>
                <Button rounded warning small>
                  <Text>Medio</Text>
                </Button>
                </TouchableOpacity>
              </Body>
              <Right>
                <Body>
                  <TouchableOpacity>
                  <Button rounded danger small>
                    <Text>Difícil</Text>
                  </Button>
                  </TouchableOpacity>
                </Body>
              </Right>
            </CardItem>
            <View style={{alignSelf:'center'}}>
              <Text>Elige la fecha de entrega</Text>
              <DatePicker
                minimumDate={new Date}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText="     DD/MM/AAAA"
                textStyle={{ color: "#000"}}
                placeHolderTextStyle={{ color: "#000" }}
                onDateChange={this.setDate}
                disabled={false}
              />
            </View>
          </Card>
          <View style={{marginTop: 20}}>
            <TouchableOpacity>
            <Button rounded warning style={{backgroundColor:'#6495ED', alignSelf:'center'}}>
              <Text>Agregar tarea</Text>
            </Button>
            </TouchableOpacity>
          </View>
      </ImageBackground>
    )
  }
}
/*
              <Button rounded style={{backgroundColor:'#6495ED', width:162, height:45}}>
              </Button>
<Button
                title="Volver a inicio"
                onPress={() => navigation.navigate('Inicio')}
              />
              */