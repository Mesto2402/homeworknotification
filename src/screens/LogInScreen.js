import React from 'react'
import{ View, Dimensions, ImageBackground, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { Button, Form, Item, Input, Label, Text } from 'native-base'
import styles from '../styles/Styles'

export default class LogInScreen extends React.Component{

  render(){
    return (
        <ImageBackground source={require('../images/HWO.jpeg')} style={{flex: 1, resizeMode: "cover"}}>
          <View
            style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 80            
            }}
          >
            <Image source={require('../images/HW_T.png')} style={{width: 150, height: 230,}}/>
          </View>
          <View
            style={{
              paddingHorizontal: 40, 
            }}
          >
          <Form>
              <Item floatingLabel regular underline >
                <Label style={{color: '#fff'}}>Usuario</Label>
                <Input />
              </Item>
              <Item floatingLabel regular underline >
                <Label style={{color: '#fff'}}>Contraseña</Label>
                <Input secureTextEntry={true}/>
              </Item>
            </Form>
            <TouchableOpacity>
            <Button rounded style={{backgroundColor: '#9AD9DB', marginTop:30, alignSelf:'center'}} onPress={() => this.props.navigation.navigate('Tabs')}>
              <Text style={{fontWeight:'bold'}}>Entrar</Text>
            </Button>
            </TouchableOpacity>
            <TouchableOpacity>
            <Button rounded style={{backgroundColor: '#fff', marginTop:50, alignSelf:'center'}} onPress={() => this.props.navigation.navigate('SignUp')}>
              <Text style={{color:'#000', fontWeight:'bold'}}>Crear cuenta</Text>
            </Button>
            </TouchableOpacity>
          </View>
        </ImageBackground>
    )
  }
}

/*
<ImageBackground source={require('../images/HWO.jpeg')} style={styles.image}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Inicia Sesion</Text>
                <Button
                  title="Entrar"
                  onPress={() => navigation.navigate('Inicio')}
                />
            </View>
          </ImageBackground>

*/