import React from 'react'
import{ View, Text, ImageBackground, StyleSheet } from 'react-native'
import { Container, Footer, FooterTab, Button, Icon, Tab, Tabs, TabHeading } from 'native-base'
//import styles from '../styles/Styles'

import Calendar from './CalendarScreen'
import Tasks from './TasksScreen'
import Profile from './ProfileScreen'
import NFC from './NFCScreen'
import Details from './DetailsScreen'

export default class HomeScreen extends React.Component{
    render(){
        const { navigation } = this.props
        
        return (
          <Container>
            <Tabs tabBarPosition='bottom' tabBarUnderlineStyle={styles.underlined}>
              <Tab heading={ <TabHeading style={styles.tabcolor}><Icon name="home" style={styles.icon}/></TabHeading>}>
                <Details />
              </Tab>
              <Tab heading={ <TabHeading style={styles.tabcolor}><Icon name="book" style={styles.icon}/></TabHeading>}>
                <Tasks />
              </Tab>
              <Tab heading={ <TabHeading style={styles.tabcolor}><Icon name="person" style={styles.icon}/></TabHeading>}>
                <Profile />
              </Tab>
              <Tab heading={ <TabHeading style={styles.tabcolor}><Icon name="calendar" style={styles.icon}/></TabHeading>}>
                <Calendar />
              </Tab>
              <Tab heading={ <TabHeading style={styles.tabcolor}><Icon name="swap" style={styles.icon}/></TabHeading>}>
                <NFC />
              </Tab>
            </Tabs>
          </Container>
        )
    }
}
const styles = StyleSheet.create({
  icon:{
    color: '#6495ed'
  },
  underlined:{
    backgroundColor:'#6495ed'
  },
  tabcolor:{
    backgroundColor:'#fff'
  },
});
/*<ImageBackground source={require('../images/HWO.jpeg')} style={styles.image}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text>Inicio</Text>
              <Button
                title="Perfil"
                onPress={() => navigation.navigate('Profile')}
              />
              <Button
                title="Tareas"
                onPress={() => navigation.navigate('Tasks')}
              />
              <Button
                title="Calendario"
                onPress={() => navigation.navigate('Calendar')}
              />
              <Button
                title="NFC"
                onPress={() => navigation.navigate('NFC')}
              />
              <Button
                title="Detalles"
                onPress={() => navigation.navigate('Details')}
              />
              <Button
                title="Salir"
                onPress={() => navigation.navigate('LogIn')}
              />
            </View>
          </ImageBackground>
          */