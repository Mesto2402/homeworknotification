import React from 'react'
import{ View, StyleSheet, Dimensions, ImageBackground, Image, TouchableOpacity } from 'react-native'
import { Container, Button, Content, Form, Item, Input, Label, Text } from 'native-base'
import styles from '../styles/Styles'

export default class SignUpScreen extends React.Component{
  render(){
    return (
      <ImageBackground source={require('../images/HWO.jpeg')} style={{flex: 1, resizeMode: "cover"}}>
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 80            
            }}
          >
            <Image source={require('../images/HW_icono.png')} style={{width: 200, height: 250,}}/>
          </View>
          <View
            style={{
              paddingHorizontal: 30,
            }}
          >
            <Text style={{color: '#fff', alignSelf:'center'}}>Crear cuenta</Text>
            <Form>
              <Item floatingLabel regular underline>
                <Label style={{color: '#fff'}}>Nombre</Label>
                <Input />
              </Item>
              <Item floatingLabel regular underline>
                <Label style={{color: '#fff'}}>Apellido</Label>
                <Input />
              </Item>
              <Item floatingLabel regular underline>
                <Label style={{color: '#fff'}}>Usuario</Label>
                <Input />
              </Item>
              <Item floatingLabel regular underline>
                <Label style={{color: '#fff'}}>Contraseña</Label>
                <Input secureTextEntry={true}/>
              </Item>
              <Item floatingLabel regular underline>
                <Label style={{color: '#fff'}}>Repetir contraseña</Label>
                <Input secureTextEntry={true}/>
              </Item>
            </Form>
            <TouchableOpacity>
            <Button rounded style={{backgroundColor: '#9AD9DB', marginTop: 30, alignSelf:'center'}} onPress={() => this.props.navigation.navigate('Tabs')}>
              <Text style={{color: '#000'}}>Crear cuenta</Text>
            </Button>
            </TouchableOpacity>
          </View>
      </ImageBackground>
    )
  }
}
