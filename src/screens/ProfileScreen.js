import React from 'react'
import { View, ImageBackground, StyleSheet, Dimensions, Image } from 'react-native'
import { Card, CardItem, Text, Button, Icon, Left, Body } from 'native-base'
import { SearchBar, Avatar, Accessory, CheckBox } from 'react-native-elements'
import { SwipeListView } from 'react-native-swipe-list-view'


export default class ProfileScreen extends React.Component {

  render(){
    const window = Dimensions.get('window')

    return (
      <ImageBackground source={require('../images/HWO.jpeg')} style={{width: window.width, height: window.height}}>
        <View
          style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 80            
          }}
        >
          <Image source={require('../images/Titulo.png')} style={{resizeMode: 'stretch', height:70, width:300}}></Image>
        </View>
        <View style={{flexDirection:'column', justifyContent:'center'}}>
          <Card transparent style={{ alignSelf:'center', width: 350}}>
            <CardItem style={{borderRadius:50, height:80, marginTop:10}}>
              <Left>
                <Avatar source={require('../images/Starrynight.jpg')} rounded size='large' activeOpacity={0.7}>
                  <Accessory />
                </Avatar>
                <Body>
                  <Text style={{fontSize:20}}>Carlos Villalobos</Text>
                  <Text note style={{color:'#000'}}>Usuario:</Text>
                  <Text note style={{color:'#000'}}>Charlievilla587</Text>
                </Body>
              </Left>
            </CardItem>
          <CardItem style={{borderRadius:50, height:80, marginTop:10}}>
              <Left>
                <Avatar source={require('../images/jony.png')} rounded size='large' activeOpacity={0.7}>
                  <Accessory />
                </Avatar>
                <Body>
                  <Text style={{fontSize:20}}>Jonatan Rodriguez</Text>
                  <Text note style={{color:'#000'}}>Usuario:</Text>
                  <Text note style={{color:'#000'}}>Jony5769</Text>
                </Body>
              </Left>
            </CardItem>
          </Card>
        </View>
      </ImageBackground>
    )
  }
}