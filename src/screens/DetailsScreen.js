import React from 'react'
import {View, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity} from 'react-native'
import {Card, CardItem, Text, Button, Icon, Left, Body, Right, Badge, Item, Input} from 'native-base'
import {SearchBar, Avatar, Accessory, CheckBox} from 'react-native-elements'
import styles from '../styles/Styles'
import {getResource, deleteDocument, getId, db} from '../initFirebase'

export default class DetailsScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            search: '',
            tasks: [],
            ids : []
        }
    }

    async init() {
        const tasks = await getResource('tasks')
        this.setState({tasks})
    }

    async delete(resource, id) {
        await deleteDocument(resource, id)
        this.init()
    }

    async keys(idsResult) {
        const ids = await idsResult
        this.setState({ids})
        this.state.ids.map(id => {
            this.delete("tasks", id)
        })
    }

    componentDidMount() {
        this.init()
    }

    updateSearch = (search) => {
        this.setState({search});
    };

    render() {
        const {navigation} = this.props
        const window = Dimensions.get('window')
        const {search} = this.state;

        return (
            <ImageBackground source={require('../images/HWO.jpeg')} style={{width: window.width, height: window.height}}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        marginTop: 80
                    }}
                >
                    <Image source={require('../images/Titulo.png')} style={{resizeMode: 'stretch', height: 70, width: 300}} />
                </View>
                <View style={{marginLeft: 30}}>
                    <Text style={{fontSize: 20, color: '#fff'}}>¡Hola Charlie!</Text>
                    <Text note style={{color: '#fff'}}>¡Te muestro tus tareas!</Text>
                </View>
                <Card transparent style={{alignSelf: 'center', width: 350}}>
                    <CardItem style={{borderRadius: 50, height: 50, marginBottom: 50}}>
                        <Item>
                            <Icon name="ios-search"/>
                            <Input placeholder="Buscar tarea"/>
                            <TouchableOpacity>
                                <Button transparent>
                                    <Icon name="paper-plane"/>
                                </Button>
                            </TouchableOpacity>
                        </Item>
                    </CardItem>
                    {
                        this.state.tasks.map(task => {

                            return <CardItem style={{ marginTop: 10 }}>
                                <Left>
                                    <Text>{task.name}</Text>
                                    <Body>
                                        <Badge danger style={{width: 10, height: 10}}/>
                                    </Body>
                                </Left>
                                <CheckBox
                                    iconType='material'
                                    checkedIcon='check'
                                    uncheckedIcon='clear'
                                    checkedColor='green'
                                    onPress={() => {
                                            this.keys(getId('tasks', 'name', task.name))
                                        }
                                    }
                                    //onPress={() => this.delete("tasks", getId('tasks', 'name', task.name))}
                                />
                            </CardItem>
                        })
                    }
                </Card>
            </ImageBackground>
        )
    }
}