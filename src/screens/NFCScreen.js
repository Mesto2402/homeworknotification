import React from 'react'
import{ View, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native'
import { Button, Text, Card, CardItem, Input, Icon, Left, Right, Body, Item, Toast } from 'native-base'
import { SearchBar } from 'react-native-elements'
import styles from '../styles/Styles'

export default class NFCScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showToast: true
    };
  }

  render(){
    const window = Dimensions.get('window')

    return (
      <ImageBackground source={require('../images/HWO.jpeg')} style={{width: window.width, height: window.height}}>
          <View
          style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 80            
          }}
          >
          <Image source={require('../images/Titulo.png')} style={{resizeMode: 'stretch', height:70, width:300}}></Image>
          </View>
          <Card transparent style={{ alignSelf:'center', width: 350}}>
            <CardItem style={{borderRadius:50, height:50}}>
              <Item>
                <Icon name="ios-search" />
                <Input placeholder="Buscar tarea a transferir" />
                <TouchableOpacity>
                <Button transparent>
                  <Icon name="paper-plane" />
                </Button>
                </TouchableOpacity>
              </Item>
            </CardItem>
            <CardItem style={{borderRadius:20, marginTop:10}}>
              <Left>
                <Body>
                  <Text>Tarea Ánalisis de Fourier</Text>
                  <Text>21/11/2021</Text>
                </Body>
              </Left>
              <TouchableOpacity>
                <Button transparent>
                  <Icon name="wifi" />
                </Button>
              </TouchableOpacity>
            </CardItem>
            <CardItem style={{borderRadius:20, marginTop:10}}>
              <Left>
                <Body>
                  <Text>Tarea Base de datos</Text>
                  <Text>17/11/2021</Text>
                </Body>
              </Left>
              <TouchableOpacity>
                <Button transparent>
                  <Icon name="wifi" />
                </Button>
              </TouchableOpacity>
            </CardItem>
            <CardItem style={{borderRadius:20, marginTop:10}}>
              <Left>
                <Body>
                  <Text>Tarea Programación móvil</Text>
                  <Text>19/11/2021</Text>
                </Body>
              </Left>
              <TouchableOpacity>
                <Button transparent>
                  <Icon name="wifi" />
                </Button>
              </TouchableOpacity>
            </CardItem>
            <CardItem style={{borderRadius:20, marginTop:10}}>
              <Left>
                <Body>
                  <Text>Tarea Gestión de conocimiento</Text>
                  <Text>01/12/2021</Text>
                </Body>
              </Left>
              <TouchableOpacity>
                <Button transparent onPress={()=> alert("holaaaaaaaa")}>
                  <Icon name="wifi" />
                </Button>
              </TouchableOpacity>
            </CardItem>
          </Card>
      </ImageBackground>
    )
  }
}