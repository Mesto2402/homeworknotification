import React from 'react';
import { StyleSheet, View } from 'react-native';
import MainStack from './src/navigation/MainStack';

class App extends React.Component {

  render (){
    return (
      <MainStack/>
    )
  }
}

export default App
